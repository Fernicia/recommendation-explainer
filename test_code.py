from Recommender import Recommender
from Explainer import Explainer
from ProfileDigest import ProfileDigest
import time
data_analiser = Recommender()
user = data_analiser.user_list[325]
data_analiser.make_active_user(user)
print(data_analiser.input_user.get_name())
explainatron = Explainer(data_analiser)
recommended_movies = data_analiser.get_k_recommended_movies(20)
start_time = time.time()
##
#best_rule = explainatron.find_best_explanation_rule(input_user, movie)
#for condition in best_rule.conditions:
#    print Rule.PREF_NAMES[condition.pref_num], explainatron.dataset_processor.get_movie_title(condition.movie_id)
#print best_rule.accuracy, best_rule.coverage
#print str(time.time() - start_time),"seconds"
##
print(len(user.prefs))
#print(ProfileDigest.list_k_favourite_films(10, user))
uniquelist = ProfileDigest.list_of_unique_opinions(user, data_analiser, uniquely_high_opinion=False)[:10]

for movie in uniquelist:
    print data_analiser.get_movie_title(movie), data_analiser.average_movie_rating(movie), user.prefs[movie]

'''
print("Standard recommender: ")

for movie in recommended_movies[:5]:
    print data_analiser.get_movie_title(movie)
rules = explainatron.generate_rules_from_movie_list(recommended_movies, data_analiser.input_user)
print "Movies with best explanations:"
for rule in rules[:5]:
    print data_analiser.get_movie_title(rule.movie_id), rule.accuracy, rule.coverage, len(rule.conditions)
'''
print(str(time.time() - start_time),"seconds")
#write function to generate n recommendations
# generate explanations for each
# rank by Accuracy
# compare with original ranked recommendation list