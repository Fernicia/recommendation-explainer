
class ProfileDigest(object):
    def __init__(self):
        pass
    @staticmethod
    def list_k_favourite_films(k, user):
        return sorted(user.prefs, key=user.prefs.get, reverse=True)[:k]
    
    @staticmethod
    def list_k_hated_films(k, user):
        return sorted(user.prefs, key=user.prefs.get)[:k]
    
    @staticmethod
    def list_of_unique_opinions(user, dataset_instance, uniquely_high_opinion=True):
        user_minus_crowd = {}
        for movie in user.prefs:
            user_minus_crowd[movie] = user.prefs[movie]-dataset_instance.average_movie_rating(movie)
        return sorted(user_minus_crowd, key=user_minus_crowd.get, reverse=uniquely_high_opinion)