from Recommender import Recommender
from Rule import Rule
from Condition import Condition
from copy import deepcopy
import time
class Explainer:
    dataset_processor = None
    def __init__(self,data_analiser):
        self.dataset_processor = data_analiser
        self.nearest_neighbours = self.dataset_processor.get_k_nearest_neighbours(50, return_pearson_tuple=False)

    def get_movies_with_shared_opinion(self, user_a, user_b):
        shared_movies = []
        for movie_id in user_a.prefs.keys():
            if self.users_agree(user_a, user_b, movie_id):
                shared_movies.append(movie_id)
        return shared_movies


    def users_agree(self, user_a, user_b, movie_id):
        if (movie_id in user_b.prefs and movie_id in user_a.prefs):
            if user_a.likes(movie_id) and user_b.likes(movie_id):
                return True
            elif user_a.is_indifferent_to(movie_id) and user_b.is_indifferent_to(movie_id):
                return True
            elif user_a.dislikes(movie_id) and user_b.dislikes(movie_id):
                return True
            else:
                return False
        else:
            False

    def get_explanation_partner(self, user, movie_id, num_users_to_consider=50):
        #pass in nearest k neighbours to avoid recalc
        candidate_explanation_partners = self.nearest_neighbours
        best_candidate = None
        for candidate in candidate_explanation_partners:
            if movie_id in candidate.prefs and candidate.likes(movie_id): #check if user LIKES movie
                if best_candidate is None or (candidate.likeness_with_active_user >
                best_candidate.likeness_with_active_user):
                    best_candidate = candidate
        return best_candidate


    def get_pref_num(self, rating):
        if rating < 3:
            return -1
        elif rating > 3:
            return 1
        else:
            return 0

    def calc_rule_accuracy(self, rule):
        rule_coverage = 0
        rule_correct = 0
        if rule is None:
            return 0, 0
        else:
            for user in self.dataset_processor.user_list:
                conditions_satisfied = 0
                for condition in rule.conditions:
                    if condition.movie_id in user.prefs and (
                       self.get_pref_num(user.prefs[condition.movie_id]) == condition.pref_num):
                        conditions_satisfied += 1
                    else:
                        break
                if conditions_satisfied == len(rule.conditions): #if users profile satisfies rule all conditions
                    rule_coverage += 1
                    if rule.movie_id in user.prefs and user.likes(rule.movie_id): #if users profile satisfies rule consequent
                            rule_correct += 1

            if rule_coverage == 0:
                return 0, 0
            else:
                return float(rule_correct)/float(rule_coverage), rule_coverage

    def get_most_accurate_rule_index(self, rule_list):
        rule_index = 0
        most_accurate_rule_index = 0
        most_accurate_rule = rule_list[most_accurate_rule_index]
        for rule in rule_list:
            new_accuracy, new_coverage = self.calc_rule_accuracy(rule)
            rule.set_accuracy(new_accuracy)
            rule.set_coverage(new_coverage)
            if (rule.accuracy > most_accurate_rule.accuracy) or (
                rule.accuracy == most_accurate_rule and rule.get_coverage() > most_accurate_rule.get_coverage()):
                most_accurate_rule = rule
                most_accurate_rule_index = rule_index
            rule_index += 1
        return most_accurate_rule_index

    def find_best_explanation_rule(self, user, explain_movie_id, num_neighbours=50):
        #deal with possibility that no one has rating > 3 (likes it)
        explanation_partner = self.get_explanation_partner(user, explain_movie_id)
        if explanation_partner is None:
            return None
        conditions = []
        potential_movies = self.get_movies_with_shared_opinion(user, explanation_partner)
        for movie_id in potential_movies:
            if movie_id != explain_movie_id:
                new_condition = Condition(self.get_pref_num(explanation_partner.prefs[movie_id]), movie_id)
                conditions.append(new_condition)
        rules = []
        for condition in conditions:
            new_rule = Rule(explain_movie_id)
            new_rule.add_condition(condition)
            rules.append(new_rule)
        if len(rules) == 0:
            return None
        best_rule_index = self.get_most_accurate_rule_index(rules)
        best_rule = rules[best_rule_index]
        del conditions[best_rule_index]
        while best_rule.accuracy < 1 and len(conditions) > 0:
            rules = []
            for condition in conditions:
                new_rule = deepcopy(best_rule)
                new_rule.add_condition(condition)
                rules.append(new_rule)
            most_accurate_rule_index = self.get_most_accurate_rule_index(rules)
            best_new_rule = rules[most_accurate_rule_index]
            del conditions[most_accurate_rule_index]
            if best_rule.accuracy >= best_new_rule.accuracy:
                return best_rule
            best_rule = best_new_rule
        return best_rule

    def generate_rules_from_movie_list(self, movie_list, user, sort_by_accuracy=True):
        rules = []
        for movie in movie_list:
            print len(rules)
            rule = self.find_best_explanation_rule(user, movie)
            if rule is not None:
                rules.append(rule)
        if sort_by_accuracy:
            rules.sort(key=lambda rule: rule.accuracy, reverse=True)
        return rules