from User import User
from scipy.stats import pearsonr
from numpy import mean
from Film import Film
class Recommender:
    film_dictionary={}
    film_list = {}
    user_list = []
    def __init__(self):
        self.import_movielens_data()
        self.input_user = None

    def make_active_user(self, input_user):
        self.input_user = input_user
        for user in self.user_list:
            user.likeness_with_active_user = self.compute_similarity(input_user, user)

    def get_k_recommended_movies(self, k, num_neighbours_to_consider=20):
        tiny_user_list = self.get_k_nearest_neighbours(num_neighbours_to_consider, return_pearson_tuple=False)
        #list of neighbour films not seen by active user
        unseen_films = set(range(1,len(self.film_list)+1)).difference(self.input_user.prefs.keys())

        recommended_movies = self.get_k_recommended_movies_from_list(unseen_films, tiny_user_list, k)
        return recommended_movies

    def compute_similarity(self, user_a, user_b, significance_weighting=True, rating_threshhold=50):
        user_a_films = user_a.prefs.keys()
        user_b_films = user_b.prefs.keys()
        #double check
        common_films = set(user_a_films).intersection(user_b_films)

        # If only 2 movies exist, pearsonr
        if len(common_films) <= 2:
            return 0
        input_user_pref_score = []
        current_user_pref_score = []
        for film_id in common_films:
            input_user_pref_score.append(user_a.prefs[film_id])
            current_user_pref_score.append(user_b.prefs[film_id])
        pearson_correlation = pearsonr(current_user_pref_score, input_user_pref_score)[0]
        if significance_weighting and len(common_films) < rating_threshhold:
            pearson_correlation *= float(len(common_films))/float(rating_threshhold)
        return pearson_correlation


    def get_k_nearest_neighbours(self, k, return_pearson_tuple=True):
        nearest_neighbours = []
        for user in self.user_list:
            if len(nearest_neighbours) < k and self.input_user.user_id != user.user_id:
                nearest_neighbours.append([user, user.likeness_with_active_user])
            elif min(nearest_neighbours, key = lambda t: t[1])[1] < user.likeness_with_active_user and self.input_user.user_id != user.user_id:
                nearest_neighbours.remove(min(nearest_neighbours, key = lambda t: t[1]))
                nearest_neighbours.append([user, user.likeness_with_active_user])
        if return_pearson_tuple == False:
            user_list_only = []
            for neighbour_pair in nearest_neighbours:
                user_list_only.append(neighbour_pair[0])
            return user_list_only
        return nearest_neighbours

    def import_movielens_data(self, path='data/hetrec2011-movielens-2k-v2'):
        for line in open(path+'/movies.dat'):
            (id_num,title) = line.split('\t')[0:2]
            movie_id = int(id_num)
            self.film_list[movie_id] = Film(movie_id, title)

        for line in open(path+'/u.info'):
            line_value = int(line.split(' ')[0])
            line_key = line.split(' ')[1].rstrip()
            if line_key == "users":
                for x in range(1, line_value+1):
                    user = User(x)
                    self.user_list.append(user)
        for line in open(path+'/user_ratedmovies_test.dat'):
            (user_id, movie_id, rating) = line.split('\t')[0:3]
            self.user_list[int(user_id)-1].rate_movie(int(movie_id), float(rating))

    def average_movie_rating(self, movie_id):
        ratings = []
        for user in self.user_list:
            if movie_id in user.prefs:
                ratings.append(user.prefs[movie_id])
        return mean(ratings)


    def average_user_rating(self, user):
        return mean(user.prefs.values())
    ##
    # Using Resnick's formula
    ##
    def predict_rating(self, film_id, neighbour_list):
        weighted_ratings = 0.0
        sum_of_pearsonr = 0
        for neighbour in neighbour_list:
            if film_id in neighbour.prefs:
                pearsonr = neighbour.likeness_with_active_user
                weighted_ratings += (neighbour.prefs[film_id] - self.average_user_rating(neighbour)) * pearsonr
                sum_of_pearsonr += pearsonr
        if sum_of_pearsonr > 0:
            predicted_rating = self.average_user_rating(self.input_user) + weighted_ratings/sum_of_pearsonr
        else:
            predicted_rating = self.average_user_rating(self.input_user)
        return predicted_rating

    def get_k_recommended_movies_from_list(self, movie_list, neighbour_list, k):
        movie_predicted_ratings = {}
        recommended_movies = []
        for movie_id in movie_list:
            movie_predicted_ratings[movie_id] = self.predict_rating(movie_id, neighbour_list)
        while len(recommended_movies) < k:
            best_scoring_movie = max(movie_predicted_ratings.iterkeys(), key=(lambda key: movie_predicted_ratings[key]))
            recommended_movies.append(best_scoring_movie)
            movie_predicted_ratings.pop(best_scoring_movie)
        return recommended_movies

    def get_movie_title(self, movie_id):
        return self.film_list[movie_id].title

