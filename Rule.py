from Condition import Condition

#Only need to recalculate accuracy when add_condition() is run
class Rule():
    consequent = None
    PREF_NAMES = {}
    PREF_NAMES[-1] = "disliked"
    PREF_NAMES[0] = "felt neutrally about"
    PREF_NAMES[1] = "liked"
    LIKES = 1
    DISLIKES = -1
    INDIFF = 0
    opinion = None
    def __init__(self, movie_id):
        self.accuracy = 0
        self.conditions = []
        self.movie_id = movie_id
        self.coverage = 0
    def get_accuracy(self):
        return self.num_users_agree / float(self.num_users_covered)
    def add_condition(self, condition):
        self.conditions.append(condition)
    def set_accuracy(self, new_accuracy):
        self.accuracy = new_accuracy
    def set_coverage(self, new_coverage):
        self.coverage = new_coverage
        
