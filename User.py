class User:
    name_prefix = "user_"
    def __init__(self, user_id):
        self.likeness_with_active_user = None
        self.user_id = user_id
        self.prefs = {}
    def rate_movie (self, movie_id, rating):
        self.prefs[movie_id] = rating
    def get_name(self):
        return self.name_prefix + str(self.user_id)
    def likes(self, movie_id):
        return self.prefs[movie_id] > 3
    def dislikes(self, movie_id):
        return self.prefs[movie_id] < 3
    def is_indifferent_to(self, movie_id):
        return self.prefs[movie_id] == 3