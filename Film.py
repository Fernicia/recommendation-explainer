class Film:
    length_in_min = 0
    def __init__(self, movie_id, title):
        self.movie_id = movie_id
        self.title = title
        self.genre = ""
        self.average_rating = 0.0

    def set_average_rating(self, rating):
        self.average_rating = rating